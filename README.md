# Android Rolling Ball Game
### with Unity Engine

---

Based on the Unity Roll-A-Ball tutorial project. Win by navigating the environment and collecting points using tilt input on a mobile device.

### Primary goals of implementation
* Built for Android phone or tablet
* Uses accelerometer input for tactile control
* Clean, stylized 3D graphics
* Testing of Unity's particle system

### To-dos:
* Sensor fusion to get linear motion in addition to rotational (trouble testing this on my phone with only accelerometer input, may need external IMU)
* Using sensor fusion data in VR/AR
* Procedural map generation
* Physics-based sound fx
* Caliberation mode

