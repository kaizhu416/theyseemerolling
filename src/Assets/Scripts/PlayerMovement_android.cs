﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement_android : MonoBehaviour
{
    public Text countText;
    public Text winText;
    public GameObject leaderDisplay;
    public Text leaderDetailText;
    public GameObject leaderInput;
    public GameObject partSys;
    private Rigidbody rb;
    private int count;
    public float speed;
    public UIButtons UIcontrol;

    private bool contact = true;
    private Vector3 linAcc, lastAcc;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";

        //HighScore.SetTestHighScore();
        HighScore.LoadHighScore();

        //ShowHighScore();
    }

    //Vector3 lastAcc;
    //new Vector3 linAcc;
    void FixedUpdate()
    {

        float moveZ = Input.acceleration.z;
        if (moveZ > 0) moveZ *= 5;
        Vector3 movement = new Vector3(0, moveZ, 0);

        Debug.Log("z-acc: " + moveZ);

        if (contact)
        {
            float moveHor = Input.acceleration.x;
            float moveVer = Input.acceleration.y;


            movement.Set(moveHor, moveZ, moveVer);
            

            /*
            if (lastAcc != null)
            {
                linAcc = Input.acceleration - lastAcc;
                rb.AddForce((linAcc * 15) * speed);
            }
            lastAcc = Input.acceleration;
            */
            
            
        }
        rb.AddForce(movement*speed);

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("pickup"))
        {
            GameObject newparticle;
            newparticle = Instantiate(partSys, transform.position, partSys.transform.rotation);
            Destroy(newparticle, 2);
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }

        //Destroy(other.gameObject);
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";

            ShowHighScore();
            StartCoroutine("WaitAndRestart");
        }

    }

    void ShowHighScore()
    {
        leaderDetailText.text = HighScore.GetHighScore();
        leaderDisplay.SetActive(true);
    }

    void OnCollisionExit(Collision collisionInfo)
    {
        contact = false;
    }

    void OnCollisionEnter(Collision collisionInfo)
    {
        contact = true;
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        contact = true;
    }

    IEnumerator WaitAndRestart() // Not void
    {
        yield return new WaitForSeconds(4); // added "return new"
        UIcontrol.ButtonRestart();
    }
}
