﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    private Text timeText;
    public float timecount;

    void Start () {
        timeText = GetComponent<Text>();
        timecount = 0;
        timeText.text = "";
    }
	
	// Update is called once per frame
	void Update () {
        timecount += Time.deltaTime;
        timeText.text = timecount.ToString("00.00") + " s";
	}
    /*
    void SetHighscore(int myHighScore)
    {

        string key = "highscore";
        int highscore = myHighscore;

        if (PlayerPrefs.HasKey(key))
            if (PlayPrefs.GetInt(key) <= highscore)
                PlayerPrefs.SetInt(key, highscore);

            else
                PlayerPrefs.SetInt(key, highscore);

        PlayerPrefs.Save();

    }
    */
}
