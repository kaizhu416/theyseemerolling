﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreRecord
{
    public string name;
    public int score;
    
    public ScoreRecord(string na, int sc)
    {
        name = na;
        score = sc;
    }
}

public static class HighScore {
    //private static int[] scores;
    //private static string[] names;

    private static List<ScoreRecord> Leaderboard;

	public static void LoadHighScore () {
        Leaderboard = new List<ScoreRecord>();

        for (int i = 0; i <= 4; i++)
        {
            Leaderboard.Add(new ScoreRecord(PlayerPrefs.GetString("leaderNamePos" + (i)), PlayerPrefs.GetInt("leaderScorePos" + (i))));
            //scores[i - 1] = PlayerPrefs.GetInt("leaderScorePos" + i);
            //names[i - 1] = PlayerPrefs.GetString("leaderNamePos" + i);
            //string temp = scores[i - 1].ToString();
            Debug.Log(Leaderboard[i].score.ToString());
        }
    }

    public static string GetHighScore()
    {
        string output = "";
        for (int i = 0; i <= 4; i++)
        {
            if (Leaderboard[i].score > 0)
            {
                output += "#" + (i + 1).ToString() + "\t" + Leaderboard[i].name + "\t" + Leaderboard[i].score.ToString() + "\n";
            }
        }
        return output;
    }

    public static int UpdateHighScore(int playerScore) {
        for (int i = 0; i <= 4; i++)
        {
            if (Leaderboard[i].score < playerScore)
            {
                Leaderboard.Insert(i, new ScoreRecord("", playerScore));
                return i;       //return highscore position
            }
        }
        return -1;  //didnt break highscore
    }
    // Update is called once per frame  
    static void SetHighScore (int rank, string playerName) {
		for (int i=0; i<=4; i++)
        {
            if (i == rank) PlayerPrefs.SetString("leaderNamePos" + i, playerName);
                else PlayerPrefs.SetString("leaderNamePos" + i, Leaderboard[i].name);
            PlayerPrefs.SetInt("leaderScorePos" + i, Leaderboard[i].score);
        }
	}

    public static void SetTestHighScore()
    {
        for (int i = 0; i <= 4; i++)
        {
            PlayerPrefs.SetString("leaderNamePos" + i, "tester"+i);
            PlayerPrefs.SetInt("leaderScorePos" + i, 100-10*i);
        }
    }
}
