﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {
    public Text countText;
    public Text winText;
    private Rigidbody rb;
    private int count;
    public float speed;
    public UIButtons UIcontrol;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
    }

    void FixedUpdate()
    { 
        float moveHor = Input.GetAxis("Horizontal");
        float moveVer = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHor, 0, moveVer);
        rb.AddForce(movement * speed);
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("pickup"))
        {
            other.gameObject.SetActive(false);
            count++;
            SetCountText();
        }
        
        //Destroy(other.gameObject);
    }

    void SetCountText()
    {
            countText.text = "Count: " + count.ToString();
        if (count >= 12)
        {
            winText.text = "You Win!";
            StartCoroutine("WaitAndRestart");
        }
        
    }

    IEnumerator WaitAndRestart() // Not void
    {
        yield return new WaitForSeconds(4); // added "return new"
        UIcontrol.ButtonRestart();
    }
}
